Cassandra
=========

This role is responsible for preparing the OS for production-ready Cassandra deployments. The role completes tasks as follows;

1. Configure Cassandra Apache repositories
2. Install Cassandra Apache Database
3. Install Cassandra Apache Prometheus Exporter
4. Disable tuned service on OS
5. Apply kernel configurations for OS
6. Disable SWAP on OS
7. Disable SELinux on OS
8. Configure rc.local for Cassandra
9. Configure Cassandra user limits on OS

Requirements
------------

To use the cassandra role you have to install and use Ansbile 

```bash
yum install epel-release -y
yum install ansible -y
```

After installed Ansible install the role as follows;

```bash
ansible-galaxy install -r requirements.yml --force
```
requirements.yml file exists on repository.


Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

```yml
---
- hosts: localhost
  connection: local
  roles:
    - cassandra

```

Author Information
------------------

- Hüseyin DEMİR
